/**
 * Created by Mateusz on 2015-11-09.
 */
var app = angular.module('app', ['materialAdmin', 'headerModule', 'homeModule'])

app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home')
    $stateProvider
        .state('home', {
            url: '/home',
            controller: 'homeController',
            templateUrl: 'app/components/home/homeView.html'
        })
    }
])