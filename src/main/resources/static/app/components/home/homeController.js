/**
 * Created by Mateusz on 2016-02-09.
 */
var homeModule = angular.module('homeModule')
homeModule.controller('homeController', ['$scope', 'growlService',
    function($scope, growlService) {
        $scope.helloWorld = 'hello world !'
        growlService.growl('Growl test', 'success')
    }
])