angular.module('materialAdmin').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('src/main/resources/static/app/components/example/exampleView.html',
    "<div class=\"block-header\"><h2>Przykład</h2></div><div class=\"card\"><div class=\"card-header\"><h2>Przykład 1 <small>Zapis/odczyt z db z wykorzystaniem hibernate'a oraz przesłanie danych do widoku z wykorzystaniem rest'owego kontrolera</small></h2></div><div class=\"table-responsive\"><table class=\"table table-condensed table-hover\" ng-show=\"examples.length > 0\"><tr><th class=\"col-md-2\">#</th><th class=\"col-md-4\">Tekst</th><th class=\"col-md-4\">Liczba</th><th class=\"col-md-2\">Usuń</th></tr><tr ng-repeat=\"example in examples\"><td>{{$index + 1}}</td><td>{{example.text}}</td><td>{{example.number}}</td><td><button class=\"btn btn-danger btn-xs waves-effect waves-circle\" ng-click=\"deleteExample(example.id)\"><i class=\"zmdi zmdi-close\"></i></button></td></tr></table></div><div class=\"card-body card-padding\"><form class=\"form-inline\"><div class=\"form-group fg-line\"><input type=\"text\" class=\"form-control\" placeholder=\"Tekst\" ng-model=\"example.text\"></div><div class=\"form-group fg-line\"><input type=\"text\" class=\"form-control\" placeholder=\"Liczba\" ng-model=\"example.number\"></div><button type=\"submit\" class=\"btn btn-primary btn-sm btn-icon-text waves-effect\" ng-click=\"addExample()\"><i class=\"zmdi zmdi-plus-circle\"></i> Dodaj</button></form></div></div><div class=\"card\"><div class=\"card-header\"><h2>Card 2</h2></div><div class=\"card-body card-padding\">card 2</div></div>"
  );


  $templateCache.put('src/main/resources/static/app/components/example2/exampleView2.html',
    "<div class=\"block-header\"><h2>Przykład</h2></div><div class=\"card\"><div class=\"card-header\"><h2>Przykład 2 <small>Wyszukiwanie danych za pomocą JDBI</small></h2></div><div class=\"card-body card-padding\"><div class=\"fg-line\"><input type=\"text\" class=\"form-control\" placeholder=\"Szukaj...\" ng-model=\"pattern\" ng-change=\"find()\"></div></div><div class=\"table-responsive\"><table class=\"table table-striped\" ng-show=\"examples.length > 0\"><tr><th class=\"col-md-2\">#</th><th class=\"col-md-5\">Tekst</th><th class=\"col-md-5\">Liczba</th></tr><tr ng-repeat=\"example in examples\"><td>{{$index + 1}}</td><td>{{example.text}}</td><td>{{example.number}}</td></tr></table></div></div>"
  );


  $templateCache.put('src/main/resources/static/app/components/home/homeView.html',
    "<div class=\"card\"><div class=\"card-header\"><h2>Home</h2></div><div class=\"card-body card-padding p-t-0\">{{helloWorld}}</div></div>"
  );

}]);
